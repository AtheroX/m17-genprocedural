﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldGen1 : MonoBehaviour{

	public float limite;

	[Header("Mesh")]
	[Range(1,100)]
	public float size = 5;
	[Range(2,256)]
	public int resolution = 8;
	Vector3 localUp;
	Vector3 axisA;
	Vector3 axisB;

	[Header("Altura")]
	public int seedHeight = 546385563;
	public int amp = 20;
	public float freq = 15;

	GameObject child;

	[Header("Cuevas")]
	public float afección = 2.5f;
	public int offset = 20;
	public int extraoffset = 5;
	[Range(0f,1f)]
	public float affeciónOffset;
	private float firstInExtraOffset = 0f;

	[Header("Mountains")]
	[Range(1,25)]
	public int octaves = 3;
	[Range(-4f,4f)]
	[Tooltip("Cuanta detalle gana cada bucle")] public float roughness = 0f;
	[Range(0f,1f)]
	[Tooltip("Cuanta importancia pierde cada bucle")] public float persistence = 0.25f;
	[Range(1, 20)]
	public int octAmp;
	public float octOffset;

	[Space]
	public bool autoUpdate = false;
	void Start(){
		autoUpdate = true;
		Generate();
	}

	public void Generate() {
		if (transform.childCount > 0) {
			for (int i = 0; i < transform.childCount; i++) {
				Destroy(transform.GetChild(i).gameObject);
			}
		}

		child = new GameObject();
		child.transform.parent = transform;

		child.AddComponent<MeshFilter>();
		child.AddComponent<MeshRenderer>().sharedMaterial = new Material(Shader.Find("Standard"));

		localUp = Vector3.back;
		axisA = new Vector3(localUp.y, localUp.z, localUp.x);
		axisB = Vector3.Cross(localUp, axisA);

		Mesh mesh = child.GetComponent<MeshFilter>().mesh;



		Vector3[] vertices = new Vector3[resolution * resolution];
		int[] tris = new int[(resolution - 1) * (resolution - 1) * 6];
		Vector2[] uv = (mesh.uv.Length == vertices.Length) ? mesh.uv : new Vector2[vertices.Length];

		float max = 0;
		float min = 100;

		int triangleIndex = 0;
		for (int x = 0; x < resolution; x++) {
			for (int y = 0; y < resolution; y++) {
				int i = x + y * resolution;
				Vector2 percent = new Vector2(x, y) / (resolution - 1);
				vertices[i] = localUp + (percent.x - .5f) * 2 * axisA * size + (percent.y - .5f) * 2 * axisB * size;

				float perl = Mathf.PerlinNoise(x / freq, (y + seedHeight) / freq) * amp;// * ((x + 1f) / resolution*afección)+offset;
				if(x < offset) {

					float s = 0, rough = roughness, amp = 1;
					for (int oct = 0; oct < octaves; oct++) {
						if (oct == 0)
							s += ((Mathf.Sin((y + seedHeight) * amp / octAmp) + rough) + 1) / 2 / (oct + 1) + octOffset;
						else
							s -= ((Mathf.Sin((y + seedHeight) * amp / octAmp) + rough) + 1) / 2 / (oct + 1) + octOffset;

						amp += persistence;
						rough *= roughness;
					}

					s -= Mathf.Sin((y + amp) / 3) / 2;

					s = Mathf.Clamp(s, -.5f, .5f);

					print(x +":"+y+" | "+ s);
					if (x<offset/2 + offset * s)
						continue;
					perl = 0;
				} else if (x < offset+extraoffset) {

					if (firstInExtraOffset == 0) {
						firstInExtraOffset = perl - x * afección;
					}
					//print(x + ":" + y + " | Original:" + perl + " Base:"+ firstInExtraOffset + " Actual:"+(Mathf.Lerp(perl - x * afección, perl, (x + 1f - offset) / (extraoffset * 1f) + affeciónOffset) +" %:"+((x+1f - offset) / (extraoffset * 1f) + affeciónOffset)));
					perl = Mathf.Lerp(firstInExtraOffset, perl, (x+1f - offset) / (extraoffset * 1f)+affeciónOffset);
				}
				if (perl > max)
					max = perl;
				if (perl < min)
					min = perl;
				if (perl > limite)
					continue;


				if (x != resolution - 1 && y != resolution - 1) {
					tris[triangleIndex] = i;
					tris[triangleIndex + 1] = i + resolution + 1;
					tris[triangleIndex + 2] = i + resolution;

					tris[triangleIndex + 3] = i;
					tris[triangleIndex + 4] = i + 1;
					tris[triangleIndex + 5] = i + resolution + 1;
					triangleIndex += 6;
				}

			}
		}

		//print(max + " " + min);

		mesh.Clear();

		mesh.vertices = vertices;
		mesh.triangles = tris;
		mesh.uv = uv;

		mesh.RecalculateNormals();
	}

}
