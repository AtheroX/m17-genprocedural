﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(WorldGen1))]
public class WorldGen1Editor : Editor{

	WorldGen1 triangle;

	public override void OnInspectorGUI(){
		using (var check = new EditorGUI.ChangeCheckScope()) {
			base.OnInspectorGUI();
			if (check.changed && triangle.autoUpdate) {
				triangle.Generate();
			}
		}

		if (GUILayout.Button("Regenerar")) {
			triangle.Generate();
		}

	}

	private void OnEnable() {
		triangle = (WorldGen1)target;
	}

}
