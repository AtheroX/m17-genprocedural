﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(WorldGen3D))]
public class WorldGen3DEditor : Editor{

	WorldGen3D wg3;

	public override void OnInspectorGUI() {
		using (var check = new EditorGUI.ChangeCheckScope()) {
			base.OnInspectorGUI();
			if (check.changed && wg3.autoUpdate) {
				wg3.Generate();
			}
		}

		if (GUILayout.Button("Regenerar")) {
			wg3.Generate();
		}

	}

	private void OnEnable() {
		wg3 = (WorldGen3D)target;
	}

}
