﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldGen3D: MonoBehaviour {

	//Terrain t;
	public int seed;
	public Vector2 offset;
    public int width = 256, height = 256;
	[Range(0.0001f,2)] public float size = 1;
	[Space]
	public float elevation = 20;
	public int octaves = 2;
	[Range(0.0001f, 1)] public float persistance = 2;
	[Range(0.0001f, 5)] public float lacunarity = 1.8f;

	public bool autoUpdate = false;
    void Start() {
		Generate();
    }

    public void Generate() {
		autoUpdate = true;
		Terrain t = GetComponent<Terrain>();
		t.terrainData = GenerateTD(t.terrainData);

	}

	TerrainData GenerateTD(TerrainData td) {
		td.heightmapResolution = width + 1;
		td.size = new Vector3(width, elevation, height);

		System.Random rng = new System.Random(seed);
		Vector2[] octSeeds = new Vector2[octaves];
		int val = 100000;
		for (int i = 0; i < octaves; i++) {
			octSeeds[i] = new Vector2(rng.Next(-val, val) + offset.x, rng.Next(-val, val) + offset.y);
		}


		float[,] h = new float[width, height];
		float maxH = float.MinValue, minH = float.MaxValue;
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {

				//float z = 0, r = 1, f = 1, totalR = 0;
				float z = 0, amp = 1, frequency = 1;
				for (int oct = 0; oct < octaves; oct++) {
					float xpos = (float) (x+offset.x) / width  + octSeeds[oct].x;
					float ypos = (float) (y + offset.y) / height + octSeeds[oct].y;
					float perl = Mathf.PerlinNoise(xpos * frequency, ypos * frequency) * 2 - 1;
					z += perl*amp ;

					amp *= persistance;
					frequency *= lacunarity;
				}
				if (z > maxH) maxH = z;
				if (z < minH) minH = z;
				h[x, y] = z;
			}
		}
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				h[x, y] = Mathf.InverseLerp(minH, maxH, h[x, y]);
			}
		}
		td.SetHeights(0, 0, h);


		td = Paint(td);
		return td;
	}

	[System.Serializable]
	public struct LayerTextures {
		public int textureID;
		public int height;
	}

	public LayerTextures[] lt;
	TerrainData Paint(TerrainData td) {
		td.alphamapResolution = width + 1;
		float[,,] alpha = td.GetAlphamaps(0, 0, width, height);

		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				float h = td.GetHeight(x, y);
				float[] s = new float[lt.Length];

				for (int i = 0; i < s.Length; i++) {
					if (i == s.Length - 1) {
						if (h >= lt[i].height)
							s[i] = 0;
					} else {
						if (h >= lt[i].height && h <= lt[i+1].height)
							s[i] = 1;

					}
				}

				for (int i = 0; i < s.Length; i++) {
					alpha[x, y, i] = s[i];
				}
			}
		}
		td.SetAlphamaps(0, 0, alpha);
		return td;
	}
}
